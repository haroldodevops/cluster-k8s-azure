resource "kubernetes_service" "teste" {
  metadata {
    name = "terraform-teste"
  }
  spec {
    selector = {
      app = kubernetes_pod.teste.metadata.0.labels.app
    }
    session_affinity = "ClientIP"
    port {
      port        = 8080
      target_port = 80
    }

    type = "LoadBalancer"
  }
}