resource "kubernetes_pod" "teste" {
  metadata {
    name = "terraform-teste"
    labels = {
      app = "MyApp"
    }
  }

  spec {
    container {
      image = "52.162.218.114:8083/login-a9ed38ee-repository:latest"
      name  = "login-a9ed38ee-repository"
    }
  }
}