resource "azurerm_container_registry" "acr" {
  name                = "containerRegistryMulticloudk8s"
  resource_group_name = azurerm_resource_group.multicloudk8s.name
  location            = azurerm_resource_group.multicloudk8s.location
  sku                 = "Basic"
  admin_enabled       = false
}
